@extends('layout.master')

@section('content')
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h5 class="m-0 font-weight-bold text-primary">Pelanggan ke :</h5>
        </div>
        <div class="card-body"> 
            <div class="row">
                <div class="col-sm-3">
                    <div>
                        <table class="table table-bordered table-sm dataTable mb-0" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                            <thead>
                                <tr><th>List Barang</th>
                            </thead>                            
                        </table>
                        <input type="text" class="form-control form-control-sm" placeholder="Search.." id="myInput">
                    </div>
                    <div style="height: 300px; overflow: auto" id="myItem">
                        <table class="table table-bordered table-hover table-sm dataTable mb-0" id="myTableItems" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                            <tbody>
                                @foreach ($items as $item)
                                <tr>
                                    <td class="listbarang" data-name="{{ $item->nama_barang }}" data-id="{{ $item->id }}" data-harga="{{ $item->harga_satuan }}">
                                        <i class="fa fa-plus"></i>                                           
                                        {{ $item->nama_barang }} 
                                    </td>
                                </tr>      
                                @endforeach                                                               
                            </table>               
                        </table>
                    </div>                    
                </div>
                <div class="col-sm-9">
                    <form action="/transaksi" method="POST">
                    @csrf
                    <table class="table table-bordered dataTable table-sm" id="cartTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                        <thead>
                            <tr><th rowspan="1" colspan="1">Id Barang</th><th rowspan="1" colspan="1">Nama Barang</th><th rowspan="1" colspan="1">Harga satuan</th><th rowspan="1" colspan="1">Jumlah</th><th rowspan="1" colspan="1">Total</th><th rowspan="1" colspan="1">Action</th>
                        </thead>
                        <tfoot>
                            <tr>
                                <th class="total">Total<input id="total" name="total" type="hidden" value="0"></th>
                                <th class="total" colspan="4" id="total_akhir"></th>
                                <th><input class="total btn btn-primary" type="submit" value="Proses" disabled></th>
                            </tr>
                        </tfoot>
                        <tbody>                    
                        </tbody>
                    </table>
                    </form>
                </div>
            </div>              
        </div>
    </div>
</div>

@endsection

@push('style')
    <style>
    
    #cartTable{
        text-align: center;
    }

    th.total{
        font-size: 25px;
    }

    input[type='number'],input[type='number']:focus {
    width: 60px;
    border: none;
    color: #6e707e;
    outline: none;
    } 
    input[type='number']::-webkit-inner-spin-button, 
    input[type='number']::-webkit-outer-spin-button { 
    opacity: 1;
    }

    .listbarang:hover {
        cursor: pointer;
    }
    /* The search field */
    #myInput {
    box-sizing: border-box;
    font-size: 16px;
    border: none;
    }

    /* The search field when it gets focus/clicked on */
    #myInput:focus {outline: 0px;}

    
    </style>
@endpush

@push('scripts')
    <script>

    $(document).ready(function(){
        
        // cari barang
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTableItems tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        // tambah barang
        $("#myTableItems td").on("click", function() {       
            var id = $(this).data("id");
            var cek_element = $("#cartTable").find("tr #"+id).length;
            var nama_barang = $(this).data("name");
            var harga_barang = $(this).data("harga").toLocaleString("id-ID");        
            var element = `
                <tr id='${id}'>
                    <td>${id}<input type='hidden' name='id_barang[]' value='${id}'></td>
                    <td>${nama_barang}</td>
                    <td id='harga${id}'>${harga_barang}<input type='hidden' name='harga_barang[]' value='${harga_barang}'></td>
                    <td><input name='jumlah_barang[]' id=${id} class='form-control-sm' type='number' min='1' value='1'></td>
                    <td class='total' id='total${id}'>${harga_barang}</td>
                    <td><button id='${id}' type='button' class='btn btn-danger btn-sm' data-name='delete'>Hapus</button></td>
                </tr>`;
            // tambah value jika barang sudah ada dlm list cart
            if(cek_element>=1){
                var harga = $(`#cartTable tr#${id} td#harga${id}`).text().replaceAll('.','');
                var jumlah = +$(`#cartTable tr#${id}`).find(":input[type='number']").val() + 1;
                var total = $(`#cartTable tr#${id} td#${id}`).text().replaceAll('.','');
                $(`#cartTable tr#${id}`).find(":input[type='number']").val(jumlah);                
                $(`#cartTable tr#${id} td#total${id}`).text((harga*jumlah).toLocaleString("id-ID"));

            }
            else{        
                $("#cartTable").append(element);
                
            }            
            // update total final
            var cek_total = $("#cartTable").find(`td.total`).length;
            var total_final = 0;
            for(let x=0;x<cek_total;x++){
                var total_sementara = Number($("#cartTable").find(`td.total`).eq(x).text().replaceAll('.','')); 
                total_final += total_sementara ;              
            }
            $(`#cartTable th#total_akhir`).text('Rp.'+(total_final).toLocaleString("id-ID"));
            $(`#cartTable`).find(':input#total').val(total_final);
            $(":submit").removeAttr("disabled");
        });  
        // update value dan toatal ketika input type number di input 
        $("#cartTable").on(`change keyup`,`:input[type='number']`,function(){               
            var id = $(this).attr("id");
            var harga = $(`#cartTable tr#${id} td#harga${id}`).text().replaceAll('.','');
            var jumlah = $(`#cartTable tr#${id}`).find(":input[type='number']").val().replaceAll('.','');
            var total = $(`#cartTable tr#${id} td#${id}`).text().replaceAll('.','');  
            if(jumlah==0){
                jumlah = 1;
                $(`#cartTable tr#${id}`).find(":input[type='number']").val(1);
            }         
            $(`#cartTable tr#${id}`).find(":input[type='number']").val(Number(jumlah));
            $(`#cartTable tr#${id} td#total${id}`).text((harga*jumlah).toLocaleString("id-ID"));
            var cek_total = $("#cartTable").find(`td.total`).length;
            var total_final = 0;
            for(let x=0;x<cek_total;x++){
                var total_sementara = Number($("#cartTable").find(`td.total`).eq(x).text().replaceAll('.','')); 
                total_final += total_sementara ;   
            }
            $(`#cartTable th#total_akhir`).text('Rp.'+(total_final).toLocaleString("id-ID"));
            $(`#cartTable`).find(':input#total').val(total_final);
        });
        // delete barang dan update total final 
        $("#cartTable").on(`click`,`:button`,function(){
            var data = $(this).data("name");
            var id = $(this).attr("id");
            if(data=='delete'){
                $("#cartTable").find(`tr#${id}`).remove();
            }
            var cek_total = $("#cartTable").find(`td.total`).length;
            var total_final = 0;
            for(let x=0;x<cek_total;x++){
                var total_sementara = Number($("#cartTable").find(`td.total`).eq(x).text().replaceAll('.','')); 
                total_final += total_sementara ;   
            }
            $(`#cartTable th#total_akhir`).text('Rp.'+(total_final).toLocaleString("id-ID"));
            $(`#cartTable`).find(':input#total').val(total_final);
            if(total_final==0){                
                $(":submit").attr("disabled",true);
            }
        });
        
    
    });
    </script>
@endpush